import pandas as pd
import numpy as np
import requests
from io import StringIO
import datetime
import configparser
import dropbox
from db_funcs import *
import psycopg2
import boto3

# get login credentials from config file
# (setting interpolation to None disables interpreting certain characters like '%' as string formatting)
config = configparser.ConfigParser(allow_no_value=True, interpolation=None)
config.read('config')

# TATARI DATA

# format the url with today's and yesterday's dates as 'YYYY-MM-DD'
# this will get all of yesterday's data
today = datetime.date.today() - datetime.timedelta(15)
yesterday = today - datetime.timedelta(1) 
start_date = yesterday.strftime("%Y-%m-%d")
end_date = today.strftime("%Y-%m-%d")

post_login_url = "https://www.tatari.tv/login"

email = config['tatari']['email']
password = config['tatari']['password']
payload = {'email': email,
    'password': password}

# format url for tatari dashboard
csv_url = f"https://www.tatari.tv/api/touchnote/report/spots?start_date={start_date}&end_date={end_date}&metric_id=Q29udmVyc2lvbk1ldHJpY1FMOjQxNQ%3D%3D"

# get data
with requests.Session() as session:
    post = session.post(post_login_url, data=payload)
    r = session.get(csv_url)

s = str(r.content,'utf-8')
data = StringIO(s) 

df = pd.read_csv(data)

# keep only date and spend columns and rename
df = df[['Date (ET)', 'Spend']]
df.columns = ["Date", "Amount"]

# drop timestamp - split on space and keep first chunk
df['Date'] = df['Date'].apply(lambda x: x.split(' ', 1)[0])

# keep only yesterday's data
df = df[df.Date==str(yesterday)]

# remove $ signs and convert spend to float
df['Amount'] = df['Amount'].apply(lambda x: float(x[1:].replace(',', '')))

# sum spend and multiply by 0.775 (to convert to GBP) and 1.095 (to add Tatari's take)
spend = round(df['Amount'].sum()*0.775*1.095, 2)

# format date
spent_date = str(yesterday)

# APPSUMER DATA

since = str(yesterday).replace('-', '')
until = str(today).replace('-', '')

apikey = config['appsumer']['apikey']

url = f"https://api.appsumer.io/v1.2/reports?token={apikey}" +\
       "&fields=partner_spend,installs,custom_event_two_dse_fifth,added_to_wishlist_dse_fifth" +\
       "&currency=gbp&decimal_places=2" +\
       "&group_by=campaign_name,sub_campaign_name,partner_name" +\
       f"&since={since}&until={until}"

response = requests.get(url)

df = pd.DataFrame(response.json()['response']['data'])

df['spent_date'] = yesterday
df['spent_date'] = pd.to_datetime(df['spent_date'])

df.rename(columns={ 
          'partner_spend': 'amount', 
          'campaign_name': 'sub_channel',
          'sub_campaign_name': 'geography',
          'partner_name': 'channel',
          "custom_event_two_dse_fifth": 'nc',
          "added_to_wishlist_dse_fifth": 'nc_from_fb'
         }, inplace=True)

df.drop(['publisher_spend_advertiser', 'publisher_spend_local'], inplace=True, axis=1)

df.fillna(0, inplace=True)

# add this to match other data
df['tv_impacts'] = 0
df['id'] = df.index

# add facebook monthly fee
fb_acquisition_fee = 5677/30

df.loc[(df.geography=='US')&(df.channel=='Facebook')&(df.sub_channel=='Acquisition'), 'amount'] += round(fb_acquisition_fee,2)

# DROPBOX FILE

# set up access to dropbox API
access_token = config['dropbox']['token']
dbx = dropbox.Dropbox(access_token)

# download marketing spend csv
metadata, data = dbx.files_download('/Looker-uploads/ALL-marketing-spends.csv')

file_data = data.content

s = str(file_data,'utf-8')

data = StringIO(s) 

db_df = pd.read_csv(data)

db_df.rename(columns={ 
          'Spent Date': 'spent_date',
          'Amount': 'amount', 
          'Group': 'sub_channel',
          'Geography': 'geography',
          'Channel': 'channel'
         }, inplace=True)

# insert tatari row into db_df 
db_df.loc[len(db_df)] = [len(db_df)+1, spend, 'TV', 'US', spent_date, 'TV', 0, 0, 0, 0]

db_df['spent_date'] = pd.to_datetime(db_df['spent_date'])

# add appsumer 
df_final = pd.concat([db_df, df], ignore_index=True, sort=False).fillna(0)

# clean up dataframe
df_final = df_final.replace({',': ''}, regex=True).replace('-', '0')

df_final.amount = round(df_final.amount.astype(float),2)

num_cols = ['nc_from_fb', 'installs', 'nc']

df_final[num_cols] = df_final[num_cols].astype(float).astype(int)

# ensure order of columns matches database table
df_final = df_final[['id', 'amount', 'channel', 'geography', 'spent_date', 'sub_channel', 'tv_impacts', 'nc_from_fb', 'installs', 'nc']]

# remove any duplicate rows (that were added manually in advance)
df_final = df_final.drop_duplicates(subset=['channel', 'geography', 'spent_date', 'sub_channel'], keep='last')

df_final['id'] = df_final.index

# save as csv
df_final.to_csv('daily_spend.csv', index=False, index_label='id')

# delete marketing spend file from dropbox
dbx.files_delete('/Looker-uploads/ALL-marketing-spends.csv')

# upload new marketing spend file to dropbox
with open('daily_spend.csv', "rb") as f:
    dbx.files_upload(f.read(), '/Looker-uploads/ALL-marketing-spends.csv', mute = True)

filename = "daily_spend_" + spent_date + ".csv"

# SAVE TO S3

csv_to_s3('daily_spend.csv', filename)

# COPY FROM S3 TO REDSHIFT

query = "CREATE TABLE marketing.marketing_spend_tmp AS select * from marketing.marketing_spends;"
query_to_df(query)

query = "truncate marketing.marketing_spend_all;"
query_to_df(query)

s3_to_db(filename)

query = "DROP TABLE marketing.marketing_spend_tmp;"
query_to_df(query)