FROM python:3.6-slim
ADD . /code
WORKDIR /code
RUN pip install --trusted-host pypi.python.org -r requirements.txt
CMD ["python", "-u", "daily_data_ingestion.py"]
