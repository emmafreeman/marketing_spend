This gets yesterday’s data from Appsumer (API) and Tatari (scraping the dashboard using Emma’s login credentials), downloads the ‘ALL-marketing-spends.csv’ file from Dropbox, inserts the updates, deletes the original file, and re-uploads the new version to Dropbox. It then stores the csv in S3 and copies to the ‘marketing.marketing_spend_all’ table in Redshift, which populates Looker.

Requires a config file with access credentials for all data sources and stores (not in git).
