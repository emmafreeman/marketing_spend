# This will run every Wednesday - it will get last weekend's data from the Tatari dashboard and update the csv in Dropbox 

import pandas as pd
import numpy as np
import requests
from io import StringIO
import datetime
import configparser
import dropbox
from db_funcs import *
import psycopg2
import boto3

# format the url with dates as 'YYYY-MM-DD'
# this will get all of the weekend's data
sunday = datetime.date.today() - datetime.timedelta(3)
friday = sunday - datetime.timedelta(2) 
saturday = sunday - datetime.timedelta(1)

start_date = friday.strftime("%Y-%m-%d")
end_date = sunday.strftime("%Y-%m-%d")

post_login_url = "https://www.tatari.tv/login"

# get login credentials for Tatari dashboard
config = configparser.ConfigParser(allow_no_value=True, interpolation=None)
# setting interpolation to None disables interpreting certain characters like '%' as string formatting
config.read('config')

email = config['tatari']['email']
password = config['tatari']['password']
payload = {'email': email,
    'password': password}

# format url for tatari dashboard
csv_url = f"https://www.tatari.tv/api/touchnote/report/spots?start_date={start_date}&end_date={end_date}&metric_id=Q29udmVyc2lvbk1ldHJpY1FMOjQxNQ%3D%3D"

# get data
with requests.Session() as session:
    post = session.post(post_login_url, data=payload)
    r = session.get(csv_url)

s = str(r.content,'utf-8')
data = StringIO(s) 

df = pd.read_csv(data)

# keep only date and spend columns and rename
df = df[['Date (ET)', 'Spend']]
df.columns = ["Date", "Amount"]

# drop timestamp - split on space and keep first chunk
df['Date'] = df['Date'].apply(lambda x: x.split(' ', 1)[0])

# Saturday's data
df_sat = df[df.Date==str(saturday)]

# remove $ signs and convert spend to float
df_sat['Amount'] = df_sat['Amount'].apply(lambda x: float(x[1:].replace(',', '')))

# sum spend and multiply by 0.775 (to convert to GBP) and 1.095 (to add Tatari's take)
spend_sat = round(df_sat['Amount'].sum()*0.775*1.095, 2)

# format date
spent_date_sat = str(saturday)

# Sunday's data
df_sun = df[df.Date==str(sunday)]

# remove $ signs and convert spend to float
df_sun['Amount'] = df_sun['Amount'].apply(lambda x: float(x[1:].replace(',', '')))

# sum spend and multiply by 0.775 (to convert to GBP) and 1.095 (to add Tatari's take)
spend_sun = round(df_sun['Amount'].sum()*0.775*1.095, 2)

# format date
spent_date_sun = str(sunday)

# set up access to dropbox API
access_token = config['dropbox']['token']
dbx = dropbox.Dropbox(access_token)

# download marketing spend csv
metadata, data = dbx.files_download('/Looker-uploads/ALL-marketing-spends.csv')

file_data = data.content

s = str(file_data,'utf-8')

data = StringIO(s) 

db_df = pd.read_csv(data)

# insert rows into df 
db_df.loc[len(db_df)] = [len(db_df)+1, spend_sat, 'TV', 'US', spent_date_sat, 'TV', 0, 0, 0, 0]
db_df.loc[len(db_df)] = [len(db_df)+1, spend_sun, 'TV', 'US', spent_date_sun, 'TV', 0, 0, 0, 0]

db_df['spent_date'] = pd.to_datetime(db_df['spent_date'])

# clean up dataframe

db_df = db_df.replace({',': ''}, regex=True).replace('-', '0')

db_df.amount = round(db_df.amount.astype(float),2)

num_cols = ['nc_from_fb', 'installs', 'nc']

db_df[num_cols] = db_df[num_cols].astype(float).astype(int)

# remove any duplicate rows
db_df = db_df.drop_duplicates(subset=['channel', 'geography', 'spent_date', 'sub_channel'], keep='last')

db_df['id'] = db_df.index

# save as csv
db_df.to_csv('daily_spend.csv', index=False, index_label='id')

# delete marketing spend file from dropbox
dbx.files_delete('/Looker-uploads/ALL-marketing-spends.csv')

# upload new marketing spend file to dropbox
with open('daily_spend.csv', "rb") as f:
    dbx.files_upload(f.read(), '/Looker-uploads/ALL-marketing-spends.csv', mute = True)

filename = "weekend_spend_" + spent_date_sun + ".csv"

# SAVE TO S3

csv_to_s3('daily_spend.csv', filename)

# COPY FROM S3 TO REDSHIFT

query = "CREATE TABLE marketing.marketing_spend_tmp AS select * from marketing.marketing_spends;"
query_to_df(query)

query = "truncate marketing.marketing_spend_all;"
query_to_df(query)

s3_to_db(filename)

query = "DROP TABLE marketing.marketing_spend_tmp;"
query_to_df(query)