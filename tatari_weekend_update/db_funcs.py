import configparser
import psycopg2
import boto3

def connect_to_db():
    """
    establishes a connection to the database using psycopg2
    """
    
    config = configparser.ConfigParser(allow_no_value=True, interpolation=None)
    # setting interpolation to None disables interpreting certain characters like '%' as string formatting
    config.read('config')
    
    # get database connection credentials
    dbname = config['redshift']['database']
    user = config['redshift']['user']
    password = config['redshift']['password']
    host = config['redshift']['host']
    port = config['redshift']['port']
    
    try:
        conn = psycopg2.connect(
                dbname=dbname,
                user=user,
                password=password,
                host=host,
                port=port
                )
    except:
        pass
    
    cur = conn.cursor()

    return conn, cur


def query_to_df(query):
    """
    runs a query on the database
    """
    conn, cur = connect_to_db()
    cur.execute(query)
    conn.commit()
    cur.close()
    conn.close()

    
def s3_creds():
    """
    gets credentials for s3
    """
    config = configparser.ConfigParser(allow_no_value=True,interpolation=None)
    config.read('config')
    access_key = config['s3']['access_key']
    secret_key = config['s3']['secret_key']
    return access_key, secret_key


def connect_to_s3():
    """
    establishes connection to S3 bucket using boto3
    """
    access_key, secret_key = s3_creds()
    
    s3 = boto3.resource(
    's3',
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key
    )
    
    return s3


def csv_to_s3(csv, filename, bucket_name='tn.retention'):
    """
    stores the csv in s3 using boto3
    """

    # store csv in S3
    s3 = connect_to_s3()
    s3.Object(bucket_name, filename).upload_file('daily_spend.csv')


def s3_to_db(filename, bucket_name='tn.retention', region_name = 'us-east-2'):
    """
    copies the csv from S3 to Redshift using psycopg2
    """
    access_key, secret_key = s3_creds()
    
    query = f"""
        copy marketing.marketing_spend_all
        from 's3://{bucket_name}/{filename}'
        ignoreheader as 1
        dateformat 'auto'
        delimiter ',' 
        csv
        access_key_id '{access_key}'
        secret_access_key '{secret_key}'
        region '{region_name}'
        ;
        """
     
    conn, cur = connect_to_db()
    cur.execute(query)
    conn.commit()
    cur.close()
    conn.close()